/*****************************************************************************
 * Copyright(c) 2010 N-Tron Corp.  All rights reserved.                      *
 *****************************************************************************
 *
 *   Creator: Dan Tappe
 *   Created: 3/3/2010 9:41
 *
 * $Revision: 11 $
 *     $Date: 11/08/11 4:47p $
 *   $Author: DTappe $
 *  $Archive: /Utilities/Diab2MsDev/Diab2MsDev.cpp $
 *
 * Description:
 * ------------
 * Originally copied from /Utilities/Microtec2MsDev/Microtec2MsDev.cpp and
 * modified to work with the Wind River Diablo (diab) compiler.
 *
 * Available Options:
 * ------------------
 *	-bp  = Bypass Parsing Routines
 *	-bs1 = Bypass Special Line 1 Parsing (Mainly For Debug)
 *  -hi  = Hide Informational Messages (Still Counted)
 *  -sh  = Show Special HAL Warnings (etoa:4949 in nt24k_hal.h)
 *  -d   = Show Line Parsing Debug
 *
 * Parsing Functions:
 * ------------------
 * Convert FROM:
 * "C:/WindRiver/components/dcom-2.3/h/dcom/comObjLib.h", line 418: warning (etoa:4997):
 *           function "IRegistry::CreateInstance(REFCLSID, IUnknown *, DWORD,
 *           const char *, ULONG, MULTI_QI *)" is hidden by
 *           "CComObject<T>::CreateInstance [with T=RemoteRegistry]" -- virtual
 *           function override intended?
 *
 * Convert TO:
 * C:/WindRiver/components/dcom-2.3/h/dcom/comObjLib.h(418) : warning etoa:4997: function "IRegistry::CreateInstance(REFCLSID, IUnknown *, DWORD, const char *, ULONG, MULTI_QI *)" is hidden by "CComObject<T>::CreateInstance [with T=RemoteRegistry]" -- virtual function override intended?
 *
 * Convert FROM:
 * "C:/WindRiver/vxworks-6.7/target/h\arch/ppc/archPpc.h", line 274: error (dcc:1173): compiler out of sync. Probably missing ';' or '}'
 *
 * Or FROM:
 * "C:/WindRiver/vxworks-6.7/target/h\arch/ppc/archPpc.h", line 274: error (dcc:1173): compiler out of sync.
 *           Probably missing ';' or '}'
 * 
 * Convert TO:
 * C:/WindRiver/vxworks-6.7/target/h\arch/ppc/archPpc.h(274) : error dcc:1173: compiler out of sync. Probably missing ';' or '}'
 *
 * Convert FROM: (No Line Number Given)
 * "C:/WindRiver/vxworks-6.7/target/h\arch/ppc/archPpc.h", warning (etoa:1609): illegal option QmsC0236
 *
 * Convert TO:
 * C:/WindRiver/vxworks-6.7/target/h\arch/ppc/archPpc.h(1) : warning etoa:1609: illegal option QmsC0236
 *
 * Known Exceptions:
 * "dld: error ..."		must be counted as error, then output line
 * "dld: warning ..."	must be counted as warning, then output line 
 * "Selected VSB incompatible with VIP ..." must be counted as error, then 
 *		"VSB: error: ...", then output line
 * "*undef*" Warning and Info - can be set to not convert Info to warning as
 *      it is just an informational warning (_NO_UNDEF_INFO_WARN)
 * "At end of source: ..." must be specially parsed and use the special functions
 *		to determine the filename to be output
 * "make ... Stop."		must be counted as error, then "make: error", then
 *		output line
 * 
 * Special Error:
 * "etoa:4949" in file "nt24k_hal.h" is output as original line (_SHOW_ORIGINAL_HAL_ERROR)
 *		or completely removed. In either case it is not counted or shown on list
 *
 * Special Line 1:
 * Determines an error/warning under some special circumstances that are otherwise
 *		missed by the parser system.
 *
 * Special Functions:
 * Grabs the Directory and Filename from the "gmake" and "gcc" lines to create
 *      a Fullname for items with no filename.
 * Recreates the Filename as a Fullname if the Filename starts with "./" to
 *      properly reset the filename for selection.
 *
 *****************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "stdafx.h"

//-----------------------//
//--- N-Tron Includes ---//
//-----------------------//

//----------------------------------------------------------------------------
//--- Macros -----------------------------------------------------------------
//----------------------------------------------------------------------------
#define _MAX_LINE_LEN	(4 * 1024)
#define _SEPS_ASM		"\";"
#define _SEPS_FILE		"\";"
#define _SEPS_ERRTYPE	"("
#define _SEPS_ERRNUM	")"
#define _SEPS			"\":;"
#define _FATAL			"fatal error"
#define _ERROR			"error"
#define _WARNING		"warning"
#define _INFO			"info"
#define _QUOTEMARK		'\"'
#define _DIR_START		"./"
#define _VXPRJ_FILE_START	"\"../"
#define _LINE_IN		", line "
#define _ERROR_OUT		"fatal error"
#define _WARNING_OUT	"warning"
#define _INFO_OUT		"informational"
#define _INFO_LINE		"  #info"
#define _CONTINUED_LINE	"          "

// Commands for File Name Extraction
#define _DCC_COMMAND	"dcc "

// Known dld Exceptions
#define _DLD_ERROR		"dld: error"
#define _DLD_WARNING	"dld: warning"

// Known make Exception
#define _MAKE_ERROR		"make: error\n"
#define _MAKE_START		"make"
#define _MAKE_STOP		"Stop."

// Other Known Exceptions
#define _VSB_INCOMPAT	"Selected VSB incompatible with VIP"
#define _VSB_ERRFORMAT	"VSB: error: %s"
#define _UNDEF_LINE		"\"*undef*\""
#define _END_OF_SOURCE	"At end of source:"

#define _ENT_DIRECTORY	" Entering directory `"

// Special Error in File Exception
#define _HAL_ERROR		"etoa:4949"
#define _HAL_FILE		"nt24k_hal.h"

// Special Line 1
#define _SPEC_LINE_1_DIRECTORY	"C:/WindRiver/"
#define _SPEC_LINE_1_FATAL		": fatal error:"
#define _SPEC_LINE_1_WARNING	": warning:"
#define _SPEC_LINE_1_ERROR		": error:"
#define _SPEC_LINE_1_INFO		": info:"

// Special Line 2
// Catch some of the arbitrary build errors encountered by Wind River Workbench.
// These may not occur once the root cause is determined and we have a correction.
// July 19, 2011

#define _SPEC_LINE_2_PERMISSION_DENIED	"Permission denied"
#define _SPEC_LINE_2_ACCESS_DENIED		"Access is denied"
#define _SPEC_LINE_2_ERROR5				"] Error 5"
#define _SPEC_LINE_2_ERROR2				"] Error 2"

// Exceptions (with _EX# suffix) to the errors above. 
//rm: cannot remove `default/BuildLog.htm': Permission denied
//rm: cannot remove `default/nt24k_application_vip.log': Permission denied

#define _SPEC_LINE_2_PERMISSION_DENIED_EX1 "BuildLog.htm': Permission denied"
#define _SPEC_LINE_2_PERMISSION_DENIED_EX2 "nt24k_application_vip.log': Permission denied"

// Define this to show all of the original HAL error in output
// #define _SHOW_ORIGINAL_HAL_ERROR

// Define this to allow first line of HAL error text in output
// if _SHOW_ORIGINAL_HAL_ERROR is undefined
// #define _SHOW_FIRST_ORIGINAL_HAL_LINE

// Undefine this to Convert "*undef*" Informationals to Warnings if
// other Informationals would be converted.
#define _NO_UNDEF_INFO_WARN

//----------------------------------------------------------------------------
//--- Types ------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//--- Globals ----------------------------------------------------------------
//----------------------------------------------------------------------------
static char _line[_MAX_LINE_LEN];
static char _tmp[_MAX_LINE_LEN];
static char _name[_MAX_LINE_LEN];
static char _directory[_MAX_LINE_LEN];
static char _errorMatches[_MAX_LINE_LEN];

//----------------------------------------------------------------------------
//--- Functions --------------------------------------------------------------
//----------------------------------------------------------------------------

int _isNum (char *x_str)
{
	size_t l_i;

	// ~FOR 
	for (l_i=0;l_i<strlen(x_str);l_i++)
	{
		//  ~IF 
		if ( !((x_str[l_i] >= '0') && (x_str[l_i] <= '9')) )
		{ //  ~THEN 
			return 0;
		} // ~ENDIF 
	} // ~ENDFOR 

	return 1;
}

//
// Main Loop, Parsing and Counting Function
//
int _tmain (int x_argc, char *x_argv[ ] )
{
	FILE *	l_in,	*l_out;
	char *	l_file;
	char *	l_num;
	char *	l_end;
	char *	l_filename;
	char *	l_temp;
	char *	szErrType;
	char *	szErrNum;
	char *	szStart;
	char*	sMatch;
	int		nErrors			= 0;
	int		nWarnings		= 0;
	int		nInformational	= 0;
	int		hide_info		= 0;	// Show Informational Messages
	int		show_hal		= 0;	// Don't Show Special HAL Errors
	int		show_debug		= 0;	// Don't Show Line Parsing Debug
	int		bypass_parse	= 0;	// Don't Bypass Parsing Routines
	int		bypass_spec_1	= 0;	// Don't Bypass Special Line 1
	int		i;
 	size_t	nLen;
	bool	prevError		= false;
	bool	prevInfo		= false;
	bool    prevUndef		= false;
	bool	UndefInfo		= false;
	bool    foundName		= false;

	// Get Any Arguments Passed In
	if (x_argc > 1)
	{
		for(i=1;i<x_argc;i++) 
		{
			// Options are:
			//  -bp  = Bypass Parsing Routines
			//	-bs1 = Bypass Special Line 1 Parsing (Mainly For Debug)
			//  -hi  = Hide Informational Messages (Still Counted)
			//  -sh  = Show Special HAL Warnings (etoa:4949 in nt24k_hal.h)
			//  -d   = Show Line Parsing Debug
			if (strcmp(x_argv[i],"-bp")==0)
			{
				bypass_parse = 1;
			}
			else if (strcmp(x_argv[i],"-bs1")==0)
			{
				bypass_spec_1 = 1;
			}
			else if (strcmp(x_argv[i],"-hi")==0)
			{
				hide_info = 1;
			}
			else if (strcmp(x_argv[i],"-sh")==0)
			{
				show_hal = 1;
			}
			else if (strcmp(x_argv[i],"-d")==0)
			{
				show_debug = 1;
			}
			else
			{
				fprintf (stderr, "only arguments are '-bp', '-bs1', '-hi', '-sh', and '-d', uses stdin, stdout\n");
				return 1;
			}
		}
	}

	// Set File I/O Variables
	l_in	= stdin;
	l_out	= stdout;

//
// Start - Loop Through All Lines
//
	_name[0] = '\0';
	_directory[0] = '\0';
	_errorMatches[0] = '\0';

	while( fgets(_line,_MAX_LINE_LEN,l_in) )
	{
		if (bypass_parse)
		{
			goto OutputLine;	// Bypass Parsing Routines
		}

		l_filename = NULL;

		if (show_debug)
		{
			// Show Line Parsing Debug
			printf ("*%.4ld:      '%s", strlen(_line), _line); fflush(l_out);
		}

//
// Start - Get Directory From Line Containing "Entering Directory"
//
		strcpy (_tmp, _line);
		szStart = _tmp;
		l_file = strtok( szStart, _SEPS);
		l_end = strtok(NULL, "");
		if ((l_end!=NULL)&&(strncmp(l_end, _ENT_DIRECTORY, strlen(_ENT_DIRECTORY)) == 0))
 		{
 			strcpy (_directory, &l_end[strlen(_ENT_DIRECTORY)]);
			_directory[strlen(_directory)-2] = '/';
			_directory[strlen(_directory)-1] = '\0';
 		}

//
// End - Get Directory From Line Containing "Entering Directory"
//

//
// Start - Get Filename From Line Starting With Command
//
		if (strncmp(_line, _DCC_COMMAND, strlen(_DCC_COMMAND)) == 0)
		{
			strcpy (_tmp, _line);
			nLen = strlen(_tmp);
			while(nLen > 0)
			{
				--nLen;

				// Remove Newline and Extra Spaces
				if ((_tmp[nLen]=='\n')||(_tmp[nLen]==' '))
					_tmp[nLen]='\0';
				else 
					break;
			}

			while(nLen > 0)
			{
				--nLen;

				// Get File Name from End of Line
				if (_tmp[nLen]==' ')
				{
					foundName = true;
	
					// Continue looking if this name is preceded by " >"
					if ((nLen>1)&&(_tmp[nLen-1]=='>')&&(_tmp[nLen-2]==' ')&&(foundName))
					{
						foundName = false;
						nLen = nLen-2;
						while(nLen > 0)
						{
							--nLen;
							if (_tmp[nLen] != ' ')
							{
								nLen++;
								_tmp[nLen] = '\0';
								break;
							}
						}
					}

						// Continue looking if this name is preceded by " -o"
					if ((nLen>2)&&(_tmp[nLen-1]=='o')&&(_tmp[nLen-2]=='-')&&(_tmp[nLen-3]==' ')&&(foundName))
					{
						foundName = false;
						nLen = nLen-3;
						while(nLen > 0)
						{
							--nLen;
							if (_tmp[nLen] != ' ')
							{
								nLen++;
								_tmp[nLen] = '\0';
								break;
							}
						}
					}
					
					if (foundName)
					{
						nLen++;
						break;
					}
				}
			}
			// Set Filename for Later Use
			_name[0] = '\0';
			strcat(_name, _directory);
			if (strncmp(&_tmp[nLen], _DIR_START, strlen(_DIR_START)) == 0)
				nLen = nLen + 2;
			strcat(_name, &_tmp[nLen]);
		}
//
// End - Get Filename From Line Starting With Command
//

		//
		// If Inside An Info Block, Remove Info Lines
		//
		if ((prevInfo)&&(!prevError))
		{
			prevInfo  = false;
			if (strncmp(_line, _INFO_LINE, strlen(_INFO_LINE)) == 0)
			{
				fgets(_line,_MAX_LINE_LEN,l_in); // Remove "#info" line
				fgets(_line,_MAX_LINE_LEN,l_in); // Remove "^" line
			}
		}
		
		//
		// If Inside An Error/Warning/Info Block, Handle Next Line As It May Be A Continuance
		//
		if (prevError)
		{
			if (strncmp(_line, _CONTINUED_LINE, strlen(_CONTINUED_LINE)) == 0)
			{
				// Continuance Of Previous Error/Warning/Info Line, Output Line
				strcpy (_tmp, _line);
				szStart = _tmp;
				while (isspace(*szStart))
					szStart++;
				strcpy(_line, szStart);
				// Remove EOL From This Line
				if (_line[strlen(_line)-1]=='\n')
					_line[strlen(_line)-1]=0;
				printf(" "); fflush(l_out);
				goto OutputLine;
			}
			else
			{
				prevError = false;
			}
			printf("\n"); fflush(l_out);	// Output EOL To Finish Previous Line
		}

		strcpy (_tmp, _line);

		szStart = _tmp;

		while (isspace(*szStart))
			szStart++;

		// If permission denied, report as error if not for certain files.
		if (((sMatch = strstr(szStart,_SPEC_LINE_2_PERMISSION_DENIED)) != NULL))
		{
			if ((strstr(szStart,_SPEC_LINE_2_PERMISSION_DENIED_EX1) == NULL) &&
				(strstr(szStart,_SPEC_LINE_2_PERMISSION_DENIED_EX2) == NULL))
			{
				nErrors++;
				strcat(_errorMatches, sMatch);
			}
		}

		if (((sMatch = strstr(szStart,_SPEC_LINE_2_ACCESS_DENIED)) != NULL) ||
			((sMatch = strstr(szStart,_SPEC_LINE_2_ERROR5)) != NULL) ||
			((sMatch = strstr(szStart,_SPEC_LINE_2_ERROR2)) != NULL))
		{
			nErrors++;
			strcat(_errorMatches, sMatch);
		}
//
// Start - Known DLD Exceptions
//
		if (strncmp(szStart, _DLD_ERROR, strlen(_DLD_ERROR)) == 0)
		{
			// Count Error, Then Output Original Line
			nErrors++;
			goto OutputLine;
		}
		else if (strncmp(szStart, _DLD_WARNING, strlen(_DLD_WARNING)) == 0)
		{
			// Count Warning, Then Output Original Line
			nWarnings++;
			goto OutputLine;
		}
//
// End - Known DLD Exceptions
//

//
// Start - Known MAKE Exception
//
		if (strncmp(szStart, _MAKE_START, strlen(_MAKE_START)) == 0)
		{
			l_temp = &_line[strlen(_line)-strlen(_MAKE_STOP)-1];
			if (strstr(l_temp, _MAKE_STOP) != 0)
			{
				// Count Error, Add Error Line, Then Output Original Line
				nErrors++;
				printf(_MAKE_ERROR);
				goto OutputLine;
			}
		}
//
// End - Known MAKE Exception
//

//
// Start - Other Known Exceptions
//
		if (strncmp(szStart, _VSB_INCOMPAT, strlen(_VSB_INCOMPAT)) == 0)
		{
			// Count Error, Add Error Line, Then Output Original Line
			nErrors++;
			printf(_VSB_ERRFORMAT, _line);
			goto OutputLine;
		}

		if (strncmp(szStart, _UNDEF_LINE, strlen(_UNDEF_LINE)) == 0)
		{
			if (prevUndef)
			{
				l_filename = _name;		// Set Filename
#ifdef _NO_UNDEF_INFO_WARN
				UndefInfo = true;
#endif
			}
			else
			{
				// Get Filename, Set Line Number, Parse Remaining Line then Bypass Parser
				l_file = strtok( szStart, _SEPS_FILE);		// Skip Filename

				l_file = _name;								// Set FileName
				
				l_num = "1";								// Set Line Number
				
				szErrType = strtok( NULL, _SEPS_ERRTYPE);	// Get Error Type
				szErrType[strlen(szErrType)-1] = '\0';
				szErrType = szErrType + 2;

				szErrNum = strtok( NULL, _SEPS_ERRNUM);		// Get Error Number

				prevUndef = true;
				goto BypassParser;
			}
		}
		prevUndef = false;

		if (strncmp(szStart, _END_OF_SOURCE, strlen(_END_OF_SOURCE)) == 0)
		{
			l_file = _name;									// Set FileName
			
			l_num  = "1";									// Set Line Number
			
			szStart += strlen(_END_OF_SOURCE);
 			szErrType = strtok( szStart, _SEPS_ERRTYPE);	// Get Error Type
 			szErrType[strlen(szErrType)-1] = '\0';
 			szErrType = szErrType + 1;

			szErrNum = strtok( NULL, _SEPS_ERRNUM);			// Get Error Number
			goto BypassParser;
		}

//
// End - Other Known Exceptions
//

		if (strncmp(szStart, _VXPRJ_FILE_START, strlen(_VXPRJ_FILE_START)) == 0)
		{
			szStart += (strlen(_VXPRJ_FILE_START) - 1);
			*szStart = _QUOTEMARK;
		}

		if (*szStart != _QUOTEMARK)
		{
//
// Start - Special Lines
// 
			if (!bypass_spec_1)
			{
				if ((strstr(szStart,_SPEC_LINE_1_FATAL) != NULL) ||
					(strstr(szStart,_SPEC_LINE_1_WARNING) != NULL) ||
					(strstr(szStart,_SPEC_LINE_1_ERROR) != NULL) ||
					(strstr(szStart,_SPEC_LINE_1_INFO) != NULL))
				{
 					bool gotit = false;
					l_file = szStart;
					for(nLen=2;nLen<strlen(l_file);nLen++)
					{
						if (l_file[nLen]==' ')
						{
							break;
						}
						if (l_file[nLen]==':')
						{
							l_file[nLen] = 0;
							l_num = strtok(&l_file[nLen+1], ":");
							gotit = true;
							break;
						}
					}
					if (!gotit) break;
					if (strncmp(l_file, _SPEC_LINE_1_DIRECTORY, strlen(_SPEC_LINE_1_DIRECTORY)) != 0)
					{
						nLen = strlen(_name);
						l_temp = "";
						while(nLen > 0)
						{
							--nLen;
							if (_name[nLen]=='/') 
							{
								l_temp = &_name[nLen+1];
								break;
							}
						}
						if (strncmp(l_temp,l_file,strlen(l_file)) == 0)
						{
							l_file = _name;
						}
						else
						{
							strcpy(_name, _directory);
							strcat(_name, l_file);
							l_file = _name;
						}
					}
					else
					{
						strcpy(_name, l_file);
					}
					szErrType = strtok( NULL, ":");
					szErrType++;
					szErrNum = ":";
					goto BypassParser;
				}
			}
//
// End - Special Lines
// 
			goto OutputLine;						// Unable To Parse, Abort - Output Original Line
		}

//
// Start - Parse Error/Warning/Info Line
//
		l_file = strtok( szStart, _SEPS_FILE);		// Get Filename
		if (l_file == NULL)
			l_file = _name;
		if (l_filename)
		{
			l_file = l_filename;
			l_filename = NULL;
		}
		else
		{
			if (strncmp(l_file, _DIR_START, strlen(_DIR_START)) == 0)
			{
				_name[0] = '\0';
				strcat(_name, _directory);
				strcat(_name, &l_file[2]);
				l_file = _name;
			}
		}

		l_num = strtok( NULL, _SEPS);				// Get Line Number
		if ( l_num == NULL )
			goto OutputLine;
		if (strncmp(l_num, _LINE_IN, strlen(_LINE_IN)) == 0)
		{
			// Good Line Number
			l_num += strlen(_LINE_IN);
			szErrType = strtok( NULL, _SEPS_ERRTYPE);	// Get Error Type
		}
		else
		{
			// Bad Line Number - Line Number Doesn't Exist In Message
			l_num[strlen(l_num)] = ':';					// Reset Line
			szErrType = strtok( l_num, _SEPS_ERRTYPE);	// Get Error Type
			szErrType++;
			l_num = "1";								// Set Line Number
		}
		szErrType[strlen(szErrType)-1] = '\0';
		szErrType++;

		szErrNum = strtok( NULL, _SEPS_ERRNUM);		// Get Error Number
//
// End - Parse Error/Warning/Info Line
//

// Bypass the Parser
BypassParser:

		if ( szErrNum == NULL )			// Bad Error Number, Abort - Output Original Line
			goto OutputLine;

// 
// Start - Special Error in File Exception
//
		if (!show_hal) 
		{
			if ( strncmp(szErrNum, _HAL_ERROR, strlen(_HAL_ERROR)) == 0)
			{
				// Strip out file name from path
				nLen = strlen(l_file);
				while(nLen > 0)
				{
					if (l_file[nLen]=='/')
					{
						l_temp = &l_file[nLen+1];
						break;
					}
					nLen--;
				}
				// Check file name
				// 11/7/2011 dst - Suppress this special error on all files, not just HAL file.
// 				if ( strncmp(l_temp, _HAL_FILE, strlen(_HAL_FILE)) == 0)
				{
#ifdef _SHOW_ORIGINAL_HAL_ERROR
					goto OutputLine;					// Don't Parse This Error - Output Original Line
#else
#ifdef _SHOW_FIRST_ORIGINAL_HAL_LINE
					fputs(_line,l_out);	fflush(l_out);	// Output First Line to screen	
#endif
					char tempc = fgetc(l_in);			// Remove All Further Lines
					while (tempc == ' ')
					{
						fgets(_line,_MAX_LINE_LEN,l_in);
						tempc = fgetc(l_in);
					}
					ungetc(tempc,l_in);
					continue;
#endif
				}
			}
		}
// 
// End - Special Error in File Exception
//

//
// Start - Count Errors, Warnings and Informations
//
		if ( strncmp(szErrType, _ERROR, strlen(_ERROR)) == 0)
		{
			// Count Error, And Set For Error Out Message
			szErrType = _ERROR_OUT;
			prevInfo = false;			// Not A Converted Info Message
			nErrors++;
		}
		else if ( strncmp(szErrType, _FATAL, strlen(_FATAL)) == 0)
		{
			// Count Error, And Set For Error Out Message
			szErrType = _ERROR_OUT;
			prevInfo = false;			// Not A Converted Info Message
			nErrors++;
		}
		else if  ( strncmp(szErrType, _WARNING, strlen(_WARNING)) == 0)
		{
			// Count Warning, And Set For Warning Out Message
			szErrType = _WARNING_OUT;
			prevInfo = false;			// Not A Converted Info Message
			nWarnings++;
		}
		else if  ( strncmp(szErrType, _INFO, strlen(_INFO)) == 0)
		{
			// Count Informational, And Set For Informational Out Message
			szErrType = _INFO_OUT;
			prevInfo = false;			// Not A Converted Info Message
			nInformational++;
#ifdef _NO_UNDEF_INFO_WARN
			if ((!hide_info)&&(!UndefInfo))
#else
			if (!hide_info)
#endif
			{
				// Set For Warning Out Message
				szErrType = _WARNING_OUT;
				prevInfo = true;		// Now Converted To Warning Message
			}
			UndefInfo = false;
		}
//
// End - Count Errors, Warnings and Informations
//

		l_end = strtok(NULL, "");

		if (_isNum(l_num) != 1)			// Bad Line Number, Abort - Output Original Line
			goto OutputLine;

//
// Start - Create Formatted Error/Warning/Info Line
//
		if (show_debug)
		{
			// Show Line Parsing Debug
			printf ("*l_file:    '%s'\n", l_file); fflush(l_out);
			printf ("*l_num:     '%s'\n", l_num); fflush(l_out);
			printf ("*szErrType: '%s'\n", szErrType); fflush(l_out);
			printf ("*szErrNum:  '%s'\n", szErrNum); fflush(l_out);
			printf ("*l_end:     '%s", l_end); fflush(l_out);
		}
		else
		{
			*_line = '\0';
			strcat (_line, l_file);
			strcat (_line, "(");
			strcat (_line, l_num);
			strcat (_line, ") : ");
			strcat (_line, szErrType);
			strcat (_line, " ");
			strcat (_line, szErrNum);
			prevError = true;				// This Line Is The First Error/Warning/Info Line
		
			//
			// Add "INFO Before Text In Informational Messages
			//
			if (prevInfo)
			{
				strcat(_line, ": INFO");
			}
	
			//
			// Handle Rest Of Error/Warning/Info Line
			//
			if ((l_end != NULL)&&(strlen(l_end)>3))
			{
				if (l_end[strlen(l_end)-1]=='\n')	// Remove EOL
					l_end[strlen(l_end)-1] = '\0';
				strcat (_line, l_end);
			}
			else
			{
				strcat (_line, ":");
			}
		}
//
// End - Create Formatted Error/Warning/Info Line
//

OutputLine:
		fputs(_line,l_out);
		fflush(l_out);					// Force each output line to screen
	}
//
// End - Loop Through All Lines
//

//
// Start - Build And Output Trailer
//
	sprintf(_line, "###  %d Error%s, %d Warning%s, %d Informational  #######################################\n", 
		nErrors, ((nErrors!=1)?"s":""), nWarnings, ((nWarnings!=1)?"s":""), nInformational);
	nLen = strlen(_line) - 1;
	_tmp[nLen] = '\0';
	while(nLen--)
		_tmp[nLen] = '#';
	strcat(_tmp, "\n");

	fputs(" \n", l_out);
	fputs(_tmp, l_out);
	fputs(_line, l_out);
	fputs(_tmp, l_out);
	fputs(" \n", l_out);
	if(strlen(_errorMatches) > 0)
	{
		fputs("Special error matches found:\n", l_out);
		fputs(_errorMatches, l_out);
		fputs(" \n", l_out);
	}
	fflush(l_out);
//
// End - Build And Output Trailer
//

	return nErrors;		// Return non-zero if we have any errors
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// $History: Diab2MsDev.cpp $ 
// 
// *****************  Version 11  *****************
// User: DTappe       Date: 11/08/11   Time: 4:47p
// Updated in $/Utilities/Diab2MsDev
// * Changed Diab2MsDev to ignore warnings about default parameters in
// function pointer prototypes in all files, not just HAL.
// 
// *****************  Version 10  *****************
// User: Fspafford    Date: 7/19/11    Time: 4:36p
// Updated in $/Utilities/Diab2MsDev
// Added some special checks for the somewhat arbitrary errors Dan and
// Frank have recently encountered. See the _SPEC_LINE_2* definitions.
// Reviewed by Dan.
// 
// *****************  Version 9  *****************
// User: Rposer       Date: 9/16/10    Time: 11:18a
// Updated in $/Utilities/Diab2MsDev
//  - Added code to have any "make ... Stop." lines be counted as an
// error.
// 
// *****************  Version 8  *****************
// User: Rposer       Date: 8/25/10    Time: 4:35p
// Updated in $/Utilities/Diab2MsDev
// Added "-bs1" option to bypass Special Line 1 parsing mainly for Debug
// Purposes
// 
// *****************  Version 7  *****************
// User: Rposer       Date: 8/25/10    Time: 3:48p
// Updated in $/Utilities/Diab2MsDev
// - Fixed bug in parsing routines
// - Modified Debug Line Output
// - Added capability to parse additional types of output lines
// - Added/Changed Arguments to select output parsing options
//   -bp = Bypass Parsing Routines
//   -hi = Hide Informational Messages (Still Counted)
//   -sh = Show Special HAL Warnings (etoa:4949 in nt24k_hal.h)
//   -d  = Show Line Parsing Debug
// 
// *****************  Version 6  *****************
// User: Rposer       Date: 8/12/10    Time: 4:48p
// Updated in $/Utilities/Diab2MsDev
// Modified to handle error/warning messages that are missing a line
// number to create a proper message for MsDev with Line Number = 1
// 
// *****************  Version 5  *****************
// User: Rposer       Date: 8/11/10    Time: 11:08a
// Updated in $/Utilities/Diab2MsDev
//  - Added define for Allowing complete bypass of parsing.
//  - Added define to allow for HAL Error to be completely removed from
// output, or to show first line only (In any case, it will not be
// counted)
// 
// *****************  Version 4  *****************
// User: Rposer       Date: 8/06/10    Time: 3:14p
// Updated in $/Utilities/Diab2MsDev
// Modified to ignore Error "etoa:4949" in file "nt24k_hal.h"
// 
// *****************  Version 3  *****************
// User: Rposer       Date: 6/16/10    Time: 3:01p
// Updated in $/Utilities/Diab2MsDev
// Added necessary code to handle a few exceptions and to properly parse
// filenames so they would work properly.
// 
// *****************  Version 2  *****************
// User: Rposer       Date: 5/26/10    Time: 2:33p
// Updated in $/Utilities/Diab2MsDev
// Improved initial work to format the Diablo output for the Visual Studio
// interface.
// 
// *****************  Version 1  *****************
// User: DTappe       Date: 5/26/10    Time: 11:31a
// Created in $/Utilities/Diab2MsDev
// Converts Wind River compiler (diab) errors to format compatible with
// Visual Studio.
// 
